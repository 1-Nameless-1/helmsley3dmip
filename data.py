from __future__ import print_function
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from generator import customImageDataGenerator
import numpy as np 
import os
import glob
import skimage.io as io
import skimage.transform as trans
from sys import exit
from PIL import Image, ImageEnhance 


def adjustData3D(img,mask,pos,organ_color_dict,cube_size):

    if(img.shape[1:] != (cube_size[0] * cube_size[1], cube_size[2], cube_size[3])):
        print("Training volume's shape and sample colume's shape don't match")
        exit()
    if(pos.shape[0] != img.shape[0]):
        print("Training volume's shape and the length of the positions don't match")
        exit()
    
    img = img / 255
    # Create an empty place holder fot the dicom image. Need to restore it from 2D image to 3D volume. 
    img = img.reshape((img.shape[0], ) + cube_size)
    #posVol has a shape of (batch_size, cube_size.x, cube_size.y, cube_size.z, 3)
    posVol = np.zeros(((pos.shape[0],) + cube_size[:-1] + (3,)))
    # fill the posVos with the pos information
    for i in range(img.shape[0]):
        posVol[i] += pos[i] 
    
    img = np.c_[img, posVol]

    if(mask is not None):

        if(mask.shape[-1] != 3):
            print("Input mask must have RGB channels.")
            exit()
        # Create an empty place holder fot the masks. Need to restore it from 2D image to 3D volume. The new mask will use one-hot encodering
        new_mask = np.zeros((mask.shape[0], ) + cube_size[:3] + (len(organ_color_dict), ), dtype="float32")
        mask = mask.reshape((mask.shape[0], ) + cube_size[:3] + (3,))
        # Filling the the new mask. 
        for i in range(len(organ_color_dict)):
            index = np.where(np.all(mask == organ_color_dict[i], axis = 4))
            new_mask[index[0], index[1], index[2], index[3], i] = 1
            #sprint("Class", i, ":", len(index[0]))

        mask = new_mask

    return (img, mask)

def adjustData3DCustom(img,mask,pos,organ_color_dict,cube_size):

    if(img.shape[1:] != (cube_size[0], cube_size[1], cube_size[2], cube_size[3])):
        print("Training volume's shape and sample colume's shape don't match")
        exit()
    if(pos != None and pos.shape[0] != img.shape[0]):
        print("Training volume's shape and the length of the positions don't match")
        exit()
    
    img = img / 255

    if(pos != None):
        #posVol has a shape of (batch_size, cube_size.x, cube_size.y, cube_size.z, 3)
        posVol = np.zeros(((pos.shape[0],) + cube_size[:-1] + (3,)))
        # fill the posVos with the pos information
        for i in range(img.shape[0]):
            posVol[i] += pos[i] 

        img = np.c_[img, posVol]

    if(mask.shape[-1] != 3):
        print("Input mask must have RGB channels.")
        exit()
    # Create an empty place holder fot the masks. Need to restore it from 2D image to 3D volume. The new mask will use one-hot encodering
    new_mask = np.zeros((mask.shape[0], ) + cube_size[:3] + (len(organ_color_dict), ), dtype="float16")
    # Filling the the new mask. 
    for i in range(len(organ_color_dict)):
        index = np.where(np.all(mask == organ_color_dict[i], axis = 4))
        new_mask[index[0], index[1], index[2], index[3], i] = 1
        #sprint("Class", i, ":", len(index[0]))

    mask = new_mask

    return (img, mask)

def adjustData2D(img,mask,organ_color_dict,sample_step=2):
    img = img / 255

    if(mask.shape[-1] != 3):
        print("Input mask must have RGB channels.")
        exit()

    # Create an empty place holder fot the adjusted masks. The new mask will use one-hot encodering
    new_mask = np.zeros(mask.shape[:3] + (len(organ_color_dict),))

    # Filling the the new mask. 
    for i in range(len(organ_color_dict)):
        index = np.where(np.all(mask == organ_color_dict[i], axis = 3))
        new_mask[index[0], index[1], index[2], i] = 1
        #sprint("Class", i, ":", len(index[0]))

    mask = new_mask

    return (img[:, ::sample_step, ::sample_step, :],mask[:, ::sample_step, ::sample_step, :])

def contrastAdjust(img):
    enhancer = ImageEnhance.Contrast(img)
    enhancer.enhance(factor)
    img_adapteq = exposure.equalize_adapthist(img, clip_limit=0.03)
    return img_adapteq

def trainGenerator(batch_size,train_path,image_folder,mask_folder,aug_dict,organ_color_dict, image_color_mode = "rgb",
                    mask_color_mode = "rgb",image_save_prefix  = "image",mask_save_prefix  = "mask",
                    multi_label = False,num_class = 2,save_to_dir = None,target_size = (256,256),seed = 1):
    '''
    can generate image and mask at the same time
    use the same seed for image_datagen and mask_datagen to ensure the transformation for image and mask is the same
    if you want to visualize the results of generator, set save_to_dir = "your path"
    '''
    image_datagen = ImageDataGenerator(**aug_dict)
    mask_datagen = ImageDataGenerator(**aug_dict)
    image_generator = image_datagen.flow_from_directory(
        train_path,
        classes = [image_folder],
        class_mode = None,
        color_mode = image_color_mode,
        target_size = target_size,
        batch_size = batch_size,
        save_to_dir = save_to_dir,
        save_prefix  = image_save_prefix,
        seed = seed)
    mask_generator = mask_datagen.flow_from_directory(
        train_path,
        classes = [mask_folder],
        class_mode = None,
        color_mode = mask_color_mode,
        target_size = target_size,
        batch_size = batch_size,
        save_to_dir = save_to_dir,
        save_prefix  = mask_save_prefix,
        seed = seed)
    train_generator = zip(image_generator, mask_generator)

    for (img,mask) in train_generator:
        img,mask = adjustData2D(img,mask, organ_color_dict)
        yield (img,mask)

def trainGenerator2D(batch_size, x, y, aug_dict,organ_color_dict,target_size = (256,256),seed = 1):
    '''
    can generate image and mask at the same time
    use the same seed for image_datagen and mask_datagen to ensure the transformation for image and mask is the same
    if you want to visualize the results of generator, set save_to_dir = "your path"
    '''
    image_datagen = ImageDataGenerator(**aug_dict)
    mask_datagen = ImageDataGenerator(**aug_dict)
    image_generator = image_datagen.flow(
        x,
        batch_size = batch_size,
        seed = seed)
    mask_generator = mask_datagen.flow(
        y,
        batch_size = batch_size,
        seed = seed)

    train_generator = zip(image_generator, mask_generator)

    for (img,mask) in train_generator:
        img,mask = adjustData2D(img,mask, organ_color_dict, sample_step=2)
        yield (img,mask)

def trainGenerator3D(batch_size, x, y, pos, aug_dict,organ_color_dict,cube_size = (64, 64, 64, 1), seed = 1):
    '''
    can generate image and mask at the same time
    use the same seed for image_datagen and mask_datagen to ensure the transformation for image and mask is the same
    if you want to visualize the results of generator, set save_to_dir = "your path"
    '''
    image_datagen = ImageDataGenerator(**aug_dict)
    mask_datagen = ImageDataGenerator(**aug_dict)
    image_generator = image_datagen.flow(
        x,
        batch_size = batch_size,
        shuffle = False, 
        seed = seed)
    mask_generator = mask_datagen.flow(
        y,
        batch_size = batch_size,
        shuffle = False,
        seed = seed)

    train_generator = zip(image_generator, mask_generator)
    counter = 0

    for (img,mask) in train_generator:

        if(counter + batch_size >= len(pos)):
            pos_input = np.concatenate((pos[counter:], pos[:counter + batch_size - len(pos)]))
            counter = counter + batch_size - len(pos)
        else:
            pos_input = pos[counter:counter+batch_size]
            counter += 1*batch_size

        img,mask = adjustData3D(img,mask, pos_input, organ_color_dict, cube_size)

        yield (img,mask)

def trainGenerator3DCustom(batch_size, x, y, pos, aug_dict,organ_color_dict,cube_size = (64, 64, 64, 1), seed = 1):
    '''
    can generate image and mask at the same time
    use the same seed for image_datagen and mask_datagen to ensure the transformation for image and mask is the same
    if you want to visualize the results of generator, set save_to_dir = "your path"
    '''
    image_datagen = customImageDataGenerator(**aug_dict)
    mask_datagen = customImageDataGenerator(**aug_dict)
    image_generator = image_datagen.flow(
        x,
        batch_size = batch_size,
        shuffle = True, 
        seed = seed)
    mask_generator = mask_datagen.flow(
        y,
        batch_size = batch_size,
        shuffle = True,
        seed = seed)

    train_generator = zip(image_generator, mask_generator)
    counter = 0

    for (img,mask) in train_generator:
        
        if(pos != None):
            # To get the pos index right
            if(counter + batch_size >= len(pos)):
                pos_input = np.concatenate((pos[counter:], pos[:counter + batch_size - len(pos)]))
                counter = counter + batch_size - len(pos)
            else:
                pos_input = pos[counter:counter+batch_size]
                counter += 1*batch_size
        else:
            pos_input = None

        newImg,newMask = adjustData3DCustom(img,mask, pos_input, organ_color_dict, cube_size)

        yield (newImg,newMask)


def testGenerator(test_path,num_image = 30,target_size = (256,256),flag_multi_class = False,as_gray = True):
    for i in range(num_image):
        img = io.imread(os.path.join(test_path,format(i, '05d') + '.png'),as_gray = as_gray)
        img = img / 255
        img = trans.resize(img,target_size)
        img = np.reshape(img,(1,)+img.shape)
        yield img

def testGeneratorTest(testX,target_size = (256,256)):
    for i in range(len(testX)):
        img = testX[i]
        img = img / 255
        img = trans.resize(img,target_size)
        img = np.reshape(img,(1,)+img.shape)
        yield img

def testGenerator3D(testX, pos, cube_size = (64, 64, 64, 1)):
    testX = testX / 255
    for i in range(len(testX)):
        #posVol has a shape of (batch_size, cube_size.x, cube_size.y, cube_size.z, 3)
        if(pos is not None):
            posVol = np.zeros(((1, ) + cube_size[:-1] + (3,)))
            # fill the posVos with the pos information
            posVol += pos[i] 

            output = np.c_[testX[i].reshape((1,) + testX[i].shape), posVol]
        else:
            output = testX[i].reshape((1,) + testX[i].shape)
        yield output

def labelVisualize(num_class,color_dict,item):
    img_out = np.zeros(item.shape[:2] + (3,))
    threshold = 0.0

    for i in range(num_class):
        # Find the category with the largest possibility of a pixel
        classLoc = np.where(np.argmax(item, axis=2) == i)
        img_out[classLoc[0], classLoc[1]] = color_dict[i]

        confidenceValues = item[classLoc[0], classLoc[1], i]
        unsureConfidenceIndex = np.where(confidenceValues < threshold)
        unsurePtsLoc = [classLoc[0][unsureConfidenceIndex], classLoc[1][unsureConfidenceIndex]]
        #print(values.shape)
        img_out[unsurePtsLoc[0], unsurePtsLoc[1]] = np.array([255, 255, 0])

        #print("Class", i, ":", len(index[0]))

    return img_out

def saveResult(save_path,npyfile,num_class:int, color_dict, img_size):
    for i,item in enumerate(npyfile):
        img = labelVisualize(num_class,color_dict,item) 
        img = img.astype('uint8')
        img = trans.resize(img, img_size)
        io.imsave(os.path.join(save_path,"%d.png"%i),img)

def saveResult3D(save_path, result, startPtsLoc, color_dict, masterVolumeSize):

    masterVolume = np.zeros(masterVolumeSize + (len(color_dict), ))
    cubeSize = result.shape[1:-1]

    # Combine all the smaller cube prediction
    catagoryStat = np.zeros((len(color_dict)))

    for i, item in enumerate(result):

        # Make the catagoty with the highest value to the one, the rest to be zero
        for j in range(len(color_dict)):
            classLoc = np.where(np.argmax(item, axis=3) == j)
            print("Category:", j, len(classLoc[0]))
            item[classLoc[0], classLoc[1], classLoc[2]] = np.zeros((len(color_dict)))
            item[classLoc[0], classLoc[1], classLoc[2], j] = 1
            catagoryStat[j] += len(classLoc[0])

        start = startPtsLoc[i]
        targetShape = masterVolume[start[0]:start[0]+cubeSize[0], start[1]:start[1]+cubeSize[1], start[2]:start[2]+cubeSize[2]].shape

        if(targetShape != item.shape):
            masterVolume[start[0]:start[0]+cubeSize[0], start[1]:start[1]+cubeSize[1], start[2]:start[2]+cubeSize[2]] \
                += item[:targetShape[0], :targetShape[1], :targetShape[2]]
        else:
            masterVolume[start[0]:start[0]+cubeSize[0], start[1]:start[1]+cubeSize[1], start[2]:start[2]+cubeSize[2]] += item

    print(catagoryStat)
    volume_out = np.zeros(masterVolumeSize + (3,))
    threshold = 0.0

    for i in range(len(color_dict)):
        # Find the category with the largest possibility of a pixel
        classLoc = np.where(np.argmax(masterVolume, axis=3) == i)
        volume_out[classLoc[0], classLoc[1], classLoc[2]] = color_dict[i]
        '''
        confidenceValues = volume_out[classLoc[0], classLoc[1],  classLoc[2], i]
        unsureConfidenceIndex = np.where(confidenceValues < threshold)
        unsurePtsLoc = [classLoc[0][unsureConfidenceIndex], classLoc[1][unsureConfidenceIndex], classLoc[2][unsureConfidenceIndex]]
        #print(values.shape)
        volume_out[unsurePtsLoc[0], unsurePtsLoc[1], unsurePtsLoc[2]] = np.array([255, 255, 0])
        '''
    for i, img in enumerate(volume_out[:]):
        img = img.astype('uint8')
        img = Image.fromarray(img, "RGB")
        img.save(os.path.join(save_path,"%d.png"%i))






        
