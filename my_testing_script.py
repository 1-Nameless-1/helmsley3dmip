from trainingLib import *
import sys

modelName = sys.argv[1]
cubeSize = (16, 128, 128)
sampleStep = (8, 64, 64)
trainX = createTestingData(1, "pa3_lava_post3", tileN=1)
sliceNum = len(trainX)
startCorPts, trainX = create3dTestingData(trainX, cubeSize=cubeSize, sampleStep=sampleStep)
testUnet(COLOR_DICT, "./model_archive/" + modelName,  cubeSize + (1, ), trainX, startCorPts, sliceNum)

trainX = createTestingData(1, "larry_2016", tileN=1)
sliceNum = len(trainX)
startCorPts, trainX = create3dTestingData(trainX, cubeSize=cubeSize, sampleStep=sampleStep)
testUnet(COLOR_DICT, "./model_archive/" + modelName, cubeSize + (1, ), trainX, startCorPts, sliceNum)