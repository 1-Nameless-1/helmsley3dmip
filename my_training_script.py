from trainingLib import *
# Run this block to prepare all the training data
x = np.load('./dicoms.npy')
y = np.load('./masks.npy')
size_list = np.load('./size.npy')

x, y, pos = create3dTrainingDataCustom(x, y, size_list, cubeSize=(16, 128, 128), sampleStep=(8, 64, 64))
trainUnet(COLOR_DICT, (16, 128, 128, 1), x, y, None, multiGpu=True)
