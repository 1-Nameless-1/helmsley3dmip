import os
from os import listdir
from os.path import isfile, join
import sys
import time
import datetime
from test_model import *
from data import *
from tensorflow.python.client import device_lib

def testUnet(colorDict, modelPath:str, input_size, trainX, startPtsLoc, sliceNum):

	print(device_lib.list_local_devices())

	'''
	Get time
	'''
	now = time.time()
	time_stamp = datetime.datetime.fromtimestamp(now).strftime('_%m_%d_%H_%M')

	'''
	input_folder = "./tmp/testingImage"
	'''
	output_folder = "./predicted_segmnetation" + time_stamp
	'''
	num_images = len([f for f in listdir(input_folder) if isfile(join(input_folder,f)) if f.endswith(".png")])

	if(num_images < 1):
		print("There is no testing images generated. Abort!")
		return -1

	'''
	'''
	Run the Test scripts
	'''
	def gen():
		return testGenerator3D(trainX, None, cube_size=input_size)
	
	imageDataShape = tf.TensorShape([1]) + input_size
	ds = tf.data.Dataset.from_generator(gen, tf.float16, imageDataShape)

	model, cpuModel = unet_3d_multi_gpu(numLabels=len(colorDict), input_size=input_size)
	model.load_weights(modelPath)

	results = model.predict(ds,verbose=1)
	'''
	Save the results
	'''
	try:
		os.mkdir(output_folder)
	except OSError:
		print ("Creation of the directory %s failed" % output_folder)
	else:
		print ("Successfully created the directory %s " % output_folder)

	saveResult3D(output_folder, results, startPtsLoc, colorDict, (sliceNum, 512, 512))



