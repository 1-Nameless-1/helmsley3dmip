import time
from datetime import datetime
from data import *
from test_model import *
from tensorflow.python.client import device_lib
from tensorflow.keras.callbacks import LambdaCallback, ModelCheckpoint, TensorBoard


def trainUnet(colorDict, input_size, x, y, pos, multiGpu:bool):

	# Check the currently available GPUs
	print(device_lib.list_local_devices())
	now = time.time()
	logdir = ".\\logs\\image\\" + datetime.now().strftime("%Y%m%d-%H%M%S")

	# Define the basic TensorBoard callback.
	tensorboard_callback = TensorBoard(log_dir=logdir)
	file_writer_pm = tf.summary.create_file_writer(logdir + '\\cm')

	'''
	Get time
	'''
	now = time.time()
	time_stamp = datetime.fromtimestamp(now).strftime('_%m_%d_%H_%M')

	'''
	This script train the model using the PNG images and labels, and save the model as hdf5 file
	'''
	image_folder = 'trainingImage'
	label_folder = 'trainingMask'
	save_folder = 'model_archive'
	model_name = 'segmentation'

	model_name = model_name + time_stamp

	'''
	Set the parameters and the model type for the training
	
	data_gen_args = dict(rotation_range=0.2,
						width_shift_range=0.05,
						height_shift_range=0.05,
						shear_range=0.05,
						zoom_range=0.05,
						horizontal_flip=False,
						fill_mode='nearest')
	'''
	data_gen_args = dict(rotation_range=20,
						width_shift_range=0.05,
						height_shift_range=0.05,
						shear_range=0.05,
						zoom_range=0.05,
						horizontal_flip=True,
						fill_mode='nearest')

	save_path = save_folder + '/' + model_name + '.hdf5'

	#myGene = trainGenerator(4,'tmp',image_folder,label_folder,data_gen_args, colorDict, save_to_dir = None, target_size=(input_size[:2]))
	if(multiGpu):
		model, cpuModel = unet_3d_multi_gpu(numLabels=len(colorDict), input_size=input_size)
	else:
		model, cpuModel = unet_3d(numLabels=len(colorDict), input_size=input_size)

	'''
	The training starts here.
	'''
	def testImage(batch, logs):
	# Use the model to predict the values from the validation dataset.
		print(batch)
		if(batch % 100 != 0):
			return

		test_vol = np.load('./testVol.npy').astype("float16")[:,:,:,0]
		test_input = test_vol.reshape((1,) + test_vol.shape + (1,))
		test_pred_raw = model.predict(test_input)[0]
		outputVol = np.zeros(test_pred_raw.shape[:-1] + (3,), dtype="uint8")
		print(outputVol.shape)

		for i in range(len(colorDict)): 
			# Find the category with the largest possibility of a pixel
			classLoc = np.where(np.argmax(test_pred_raw, axis=3) == i)
			outputVol[classLoc[0], classLoc[1], classLoc[2]] = colorDict[i]


		testImages = test_input[0].astype('uint8')
		testMasks = outputVol.astype('uint8')

		with file_writer_pm.as_default():
			tf.summary.image("testImage", testImages, step=batch)
			tf.summary.image("testMask", testMasks, step=batch)

	pm_callback = LambdaCallback(on_batch_begin=testImage)
	cp_callback = ModelCheckpoint(filepath='/tmp/weights.hdf5', verbose=1, save_best_only=True)

	batch_size = 8

	def gen():
		return trainGenerator3DCustom(batch_size, x, y, pos, data_gen_args, colorDict, cube_size=input_size)

	maskDataShape = tf.TensorShape([batch_size]) + input_size[:-1] + (len(colorDict), )
	imageDataShape = tf.TensorShape([batch_size]) + input_size

	ds = tf.data.Dataset.from_generator(gen, (tf.float16, tf.float16), (imageDataShape, maskDataShape))

	model.fit(ds, steps_per_epoch=10000, epochs=3, callbacks=[tensorboard_callback, cp_callback])
	
	#model.fit_generator(myGene,steps_per_epoch=4096,epochs=5)

	if(cpuModel):
		cpuModel.save(save_path)
	else:
		model.save(save_path)








